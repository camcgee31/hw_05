#!/bin/bash
# For this assinment I will change how the text book did the code but I will also add in a few things

#for my code I will use read commands to get user input.
echo "what is the temperature?"
read temp

echo "What is the temperature unit [F|C|K]"
read unit

#If temperature unit = F(Fharenheit) then
# set farn to temperature 
# set cels (echo scal to 2 to allow 2 values to the right of the decimal then echo farn - 32 then devide that by 1.8 then pip it into bc whitch is an valculator language)
# set kelvin (echo scal to 2 to allow 2 values to the right of the decimal then calculate $cels + 273.15 then pip it into bc)
# echo the values and how we got the values
if [ "$unit" = "F" ]
then
	farn="$temp"
	cels="$(echo "scale=2;($farn - 32) / 1.8" | bc)"
	kelv="$(echo "scale=2;$cels + 273.15" | bc)"
	echo "Fahrenheit = $farn"
	echo "Celsius = $cels"
	echo "Celsius: ($farn - 32) / 1.8 = $cels"
	echo "Kelvin = $kelv"
	echo "Kelvin: $cels + 273.15 = $kelv"
#If temperature unit = C(Celsius) then
# set farn (echo scal to 2 to allow 2 values to the right of the decimal then calculate (1.8 * $cels) + 32 then pip it into bc whitch is an valculator language)
# set kelvin (echo scal to 2 to allow 2 values to the right of the decimal then calculate $cels + 273.15 then pip it into bc)
# echo the values and how we got the values
elif [ "$unit" = "C"  ]
then
	cels=$temp
	farn="$(echo "scale=2;(1.8 * $cels) + 32" | bc)"
	kelv="$(echo "scale=2;$cels + 273.15" | bc)"
	echo "Fahrenheit = $farn"
	echo "Fharenheit: ($cels * 1.8) + 32"
	echo "Celsius = $cels"
	echo "Kelvin = $kelv"
	echo "Kelvin: $cels + 273.15 = $kelv"
#If temperature unit = C(Celsius) then
# set farn (echo scal to 2 to allow 2 values to the right of the decimal then calculate (1.8 * $cels) + 32 then pip it into bc whitch is an valculator language)
# set cels (echo scal to 2 to allow 2 values to the right of the decimal then echo farn - 273.15 then devide that by 1.8 then pip it into bc whitch is an valculator language)
elif [ "$unit" = "K"  ]
then
	kelv=$temp
	cels="$(echo "scale=2; $kelv - 273.15" | bc)"
	farn="$(echo "scale=2; (1.8 * $cels) + 32" | bc)"
	echo "Fahrenheit = $farn"
	echo "Fharenheit: (1.8 * $cels) + 32"
	echo "Celsius = $cels"
	echo "Celsius: ($farn - 32) / 1.8 = $cels"
	echo "Kelvin = $kelv"
else
	echo "Unit not support please use F(Fharenheit) C(Celsius) K(Kelvin)"
fi


